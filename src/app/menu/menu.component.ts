import { Component } from '@angular/core';
import { PizzaService } from './pizza.service';
import { Pizza } from './pizza';
import { ShoppingCartComponent } from "../shopping-cart/shopping-cart.component";
import { ShoppingCartService } from '../shopping-cart/shoppingService/shopping-cart.service';

@Component({
  selector: 'app-menu',
  standalone: true,
  templateUrl: './menu.component.html',
  styleUrl: './menu.component.css',
  imports: [ShoppingCartComponent]
})
export class MenuComponent {
  pizzas: Pizza[];

  constructor(private pizzaService: PizzaService, private shoppingCartService: ShoppingCartService) {
    this.pizzas = this.pizzaService.getAllPizzas();
  }

  addPizzaToShoppingCart(pizza: Pizza): void {
    this.shoppingCartService.addPizza(pizza);
  }
}
