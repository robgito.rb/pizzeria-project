export interface Pizza {
    name: string;
    description: string;
    toppings: string[];
    price: number;
    photoUrl: string;
}
