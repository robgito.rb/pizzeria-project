import { Injectable } from '@angular/core';
import { Pizza } from './pizza';

@Injectable({
  providedIn: 'root'
})
export class PizzaService {
  pizzas: Pizza[] = [
    {
      name: 'Margherita',
      description: 'Classic Italian pizza with tomato sauce and mozarella cheese',
      toppings: ["tomato sauce", "mozzarella", "cheese"],
      price: 9.99,
      photoUrl: 'https://www.dominos.jp/ManagedAssets/JP/product/90/JP_90_en_hero_991.png?v265798167'
    },
    {
      name: 'Pepperoni',
      description: 'Pizza topped with pepperoni slices',
      toppings: ["tomato sauce", "mozzarella", "cheese", "pepperoni"],
      price: 11.99,
      photoUrl: 'https://www.cicis.com/media/i2fjnp22/zesty-pepperoni-pizza.png'
    },
    {
      name: 'Hawaiian',
      description: 'Pizza with tomato sauce, mozzarella, cheese, ham and pineapple',
      toppings: ["tomato sauce", "mozzarella", "cheese", "ham", "pineapple"],
      price: 12.99,
      photoUrl: 'https://www.cicis.com/media/nc3fo1w5/hawaiian-pizza.png'
    },
    {
      name: 'Meat Lover\'s',
      description: 'Pizza with tomato sauce, mozzarella, cheese, pepperoni, sausage, bacon and ham',
      toppings: ["tomato sauce", "mozzarella", "cheese", "ham", "pepperoni", "bacon", "sausage"],
      price: 14.99,
      photoUrl: 'https://www.cicis.com/media/b4zhf0hf/meat-eater-pizza.png'
    },
    {
      name: 'Vegetarian',
      description: 'Pizza with tomato sauce, mozzarella, cheese, mushrooms, onions, peppers and olives',
      toppings: ["tomato sauce", "mozzarella", "cheese", "mushrooms", "onions", "peppers", "olives"],
      price: 12.99,
      photoUrl: 'https://www.cicis.com/media/chtdvcdp/zesty-veggie-pizza.png'
    },
    {
      name: 'BBQ chicken',
      description: 'Pizza with BBQ sauce, mozzarella, cheese, grilled chicken and red onions',
      toppings: ["BBQ sauce", "mozzarella", "cheese", "grilled chicken", "red onions"],
      price: 13.99,
      photoUrl: 'https://www.cicis.com/media/3kebsgza/bbq-pork-pizza.png'
    },
  ];

  constructor() { }

  getAllPizzas(): Pizza[] {
    return this.pizzas;
  }
}
