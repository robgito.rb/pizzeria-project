import { Component } from '@angular/core';
import { CartItem } from './shoppingService/cart-item';
import { ShoppingCartService } from './shoppingService/shopping-cart.service';

@Component({
  selector: 'app-shopping-cart',
  standalone: true,
  imports: [],
  templateUrl: './shopping-cart.component.html',
  styleUrl: './shopping-cart.component.css'
})
export class ShoppingCartComponent {
  cartItems: CartItem[];

  constructor(private shoppingcartService: ShoppingCartService) {
    this.cartItems = this.shoppingcartService.getCartItems();
  }

  getTotalPrice(): number {
    return this.shoppingcartService.calcTotalPrice();
  }
}
